<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    // public function welcome()
    // {
    //     return view('welcome');
    // }
    public function welcome(Request $request)
    {
        $namaDepan = $request['depan']; 
        $namaBelakang = $request['belakang'];

        return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
